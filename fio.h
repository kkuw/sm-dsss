#include <fstream>
#include <iostream>
#include <string>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>

#include "error.h"


#define COMPRESSION_LEVEL 6


using namespace std;

class FileReader {

 private:
  std::ifstream file;
  bool isCompressed;
  boost::iostreams::filtering_streambuf<boost::iostreams::input> gzin;
  std::istream *input;

 public:
  istream& getInput(string&);
  void clear();
  FileReader(string);
  ~FileReader();
};

class FileWriter {

 private:
  std::ofstream file;
  bool isCompressed;
  boost::iostreams::filtering_ostream gzout;

 public:

  void write(char *);
  void write(string&);
  void close();
  FileWriter(string);
  ~FileWriter();
};

bool isg(string fname);
void openInFile(ifstream *,string,std::ios_base::openmode);
void openOutFile(ifstream *,string,std::ios_base::openmode);

 
