#Example script for 2-step submodular data selection 
#Input: list of training files, test/heldout text file, output directory, budget for first step, budget for second step 
#!/bin/bash 

if [ $# -lt 5 ]
then
    echo "Usage: run.sh <list of train src files> <test file> <tmpdir> <1st step budget> <2nd step budget>" 
    exit
fi

DIR=/g/ssli/projects/afrl/select-lm/software/dsss
# ngram-order 
order=5 

TRAINLIST=$1
TESTFILE=$2 
TMPDIR=$3
B1=$4
B2=$5

if [ ! -d $TMPDIR ]
then
    mkdir $TMPDIR
fi

echo $TESTFILE > test.list

#extract query (test) set features

echo "Extracting query set features..."
cmd="$DIR/extract_features -order $order -input test.list -stats $TESTFILE"
echo $cmd
eval $cmd 

#extract train features
echo "Extracting training features"
cp $TRAINLIST step1.train.list 
cmd="$DIR/extract_features -query $TESTFILE.stats -order $order -input step1.train.list -stats allfeatures" 
echo $cmd
eval $cmd

NFEA=`wc -l allfeatures.stats | gawk '{print $1}'`

#first step: extract B1 % from each training file - parallelize this step!

rm -f $TMPDIR/train.$B1.txt $TMPDIR/train.$B1.features
TRAIN2=$TMPDIR/train.$B1
for file in `cat $TRAINLIST`
do
    NUTT=`cat $file.costs | wc -l | gawk '{print $1}'` 
    cmd="$DIR/select_data -m $NUTT -n $NFEA -per $B1 -out $file.$B1.ids -stats allfeatures.stats -costs $file.costs -vset $file.features -beta 0.5 -alpha 0.2 -fratio -tfidf &> $file.select.log"
    echo $cmd
    eval $cmd
    $DIR/extract_sents -ids $file.$B1.ids -input $file -output $file.$B1.txt
    $DIR/extract_sents -ids $file.$B1.ids -input $file.costs -output $file.$B1.costs
    $DIR/extract_sents -ids $file.$B1.ids -input $file.features -output $file.$B1.features
    cat $file.$B1.txt >> $TRAIN2.txt
    cat $file.$B1.features >> $TRAIN2.features
    cat $file.$B1.costs >> $TRAIN2.costs
done

#second step


#select final set according to budget B2

m=`wc -l $TRAIN2.features | gawk '{print $1}'` 

cmd="$DIR/select_data -n $NFEA -m $m -per $B2 -out $TRAIN2.$B2.ids -stats allfeatures.stats -costs $TRAIN2.costs -vset $TRAIN2.features -beta 0.5 -alpha 0.2 -fratio -tfidf &> $TRAIN2.select.log "

echo $cmd
eval $cmd

echo "Compiling final training set"
$DIR/extract_sents -ids $TRAIN2.$B2.ids -input $TRAIN2.txt -output $TRAIN2.final 

echo "Final selected text is in $TRAIN2.final"










  



