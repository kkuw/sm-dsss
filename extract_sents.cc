#include <boost/algorithm/string.hpp>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <stdlib.h>

#include "math.h"
#include "time.h"

#include "fio.h" 
#include "utils.h"
#include "error.h"
#include "arguments.h"

using namespace std;
namespace io = boost::iostreams;

#define MAXBUF 100000
#define MAXLEN 248

char *output_fname = NULL;
char *input_fname = NULL;
char *id_fname = NULL;
char *help=NULL;
double budget = 0.0;

Arg Arg::Args[]={
  Arg("ids",Arg::Req,id_fname,"file with sentence ids", Arg::SINGLE),
  Arg("budget",Arg::Opt,budget,"upper limit of num words to select", Arg::SINGLE),
  Arg("output",Arg::Req,output_fname,"output file name",Arg::SINGLE),
  Arg("input",Arg::Req,input_fname,"input file name",Arg::SINGLE),
  Arg("help", Arg::Help, help, "Print this message"), 
  Arg()
};

int
main(int argc, char **argv) {


  boost::unordered_set<unsigned int> use;

  bool parse_was_ok = Arg::parse(argc,(char**)argv);

  if (!parse_was_ok){
    Arg::usage(); exit(-1);
  }

  // read ids

  string id_file(id_fname);
  string input_file(input_fname);
  string output_file(output_fname);


  FileReader ids(id_file);
  FileReader data(input_file);
  FileWriter outdata(output_file);


  string line;
  unsigned int i = 0;
  unsigned int n = 0;

  while (ids.getInput(line)) {
    i = atoi(line.c_str());
    use.insert(i);
    n++;
  }
  ids.clear(); 

  long unsigned int l = 0;
  double total_words = 0;
  long unsigned int inserted = 0;
  int nw = 0; 

  while (data.getInput(line)) {
    if (use.find(l) != use.end()) { 
      nw = CountWords((char *)line.c_str()); 
      line += "\n";
      outdata.write(line);
      total_words += (double)nw;
      inserted++;
    }
    if (budget > 0 && total_words > budget) {
	data.clear();
	exit(0);
    }
    l++;
  }
  data.clear();
  printf("Selected %li lines, %f words\n",inserted,total_words);
  return 0;
}

























