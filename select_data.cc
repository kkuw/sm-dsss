#include <map>
#include <queue>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/resource.h>	 
#include <ctime>
#include <string>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include <boost/algorithm/string.hpp>
#include "stdlib.h"
#include "math.h"	 
#include "time.h"
#include "arguments.h"
#include "fio.h"
#include "utils.h"
#include "error.h"

using namespace std;

#define SMALLEST_NUMBER -1e70
#define LARGEST_NUMBER 1e70

unsigned int nUtt = 0;
unsigned int nFeat = 0; 
double per = 0;
char* outFileName=NULL;
char* statFileName=NULL;
char* weightFileName=NULL;
char* costFileName=NULL;
char* confFileName=NULL;
char* trainFileName=NULL;
bool tfidf = false;
bool fratio = false;
bool useWeights = false;
double beta = 1.0;
double alpha = 0.0;
bool useTFIDF = false;
double prune_factor = 100.0;
const char* concaveFunc="sqrt";

char* help=NULL;
int verb=0;
double lweight=1.0;

typedef boost::unordered_map<int, double> hashmap; 
typedef boost::unordered_map<unsigned int,hashmap> vset;
typedef boost::unordered_map<unsigned int,double> FeatureMap;  // maps between feature id and some value

bool cmp(double a, double b) { return a>b;}

Arg Arg::Args[]={
        Arg("m", Arg::Req, nUtt, "the total number of sentences",Arg::SINGLE),
        Arg("n", Arg::Req, nFeat, "the total number of features",Arg::SINGLE),
	Arg("per", Arg::Req, per, "the percentage of data to be selected",Arg::SINGLE),
        Arg("out", Arg::Req, outFileName, "the output file",Arg::SINGLE),
	Arg("weights", Arg::Opt, weightFileName, "feature weight file", Arg::SINGLE),
	Arg("stats", Arg::Req, statFileName, "feature stats file", Arg::SINGLE),
	Arg("costs", Arg::Req, costFileName, "sentence cost file", Arg::SINGLE),
	Arg("conf", Arg::Opt, confFileName, "sentence confidence file", Arg::SINGLE),
	Arg("vset", Arg::Req, trainFileName, "feature count file for V", Arg::SINGLE),
	Arg("alpha", Arg::Opt, alpha, "regularizer of cardinality", Arg::SINGLE),
	Arg("beta", Arg::Opt, beta, "maximum gain slack factor", Arg::SINGLE),
	Arg("fratio", Arg::Opt, fratio, "use frequency ratio", Arg::SINGLE),
	Arg("prune", Arg::Opt, prune_factor, "use only X percent of ground set", Arg::SINGLE),
	Arg("tfidf", Arg::Opt, useTFIDF, "use TF-IDF weighting for features", Arg::SINGLE),
        Arg("verb", Arg::Opt, verb, "verbosity",Arg::SINGLE),
        Arg("help", Arg::Help, help, "Print this message"), 
        Arg()
};

class Increment{
public: 
  Increment(){};
  Increment(double x, int i){ value=x; index=i; }
  bool operator< (const Increment&) const;
	
  int get_index() const { return index; }
  double get_value() const{ return value; }
private:
  int index;
  double value;
};

bool Increment::operator< (const Increment& right) const
{
	return value < right.value;
}

long GetFileSize(const char * filename )
{
    struct stat statebuf;

    if ( stat( filename, &statebuf ) == -1 )
        return -1L;
    else
        return statebuf.st_size;
}

void reportTiming(
                  const struct rusage& rus,
                  const struct rusage& rue) {

  struct timeval utime;
  double utimef;
  struct timeval stime;
  double stimef;

  /* user time */
  utime.tv_sec = rue.ru_utime.tv_sec - rus.ru_utime.tv_sec ;
  if ( rue.ru_utime.tv_usec < rus.ru_utime.tv_usec ) {
    utime.tv_sec--;
    utime.tv_usec = 1000000l - rus.ru_utime.tv_usec +
      rue.ru_utime.tv_usec;
  } else
    utime.tv_usec = rue.ru_utime.tv_usec -
      rus.ru_utime.tv_usec ;
  utimef = (double)utime.tv_sec + (double)utime.tv_usec/1e6;

  /* system time */
  stime.tv_sec = rue.ru_stime.tv_sec - rus.ru_stime.tv_sec ;
  if ( rue.ru_stime.tv_usec < rus.ru_stime.tv_usec ) {
    stime.tv_sec--;
    stime.tv_usec = 1000000l - rus.ru_stime.tv_usec +
      rue.ru_stime.tv_usec;
  } else
    stime.tv_usec = rue.ru_stime.tv_usec -
      rus.ru_stime.tv_usec ;

  stimef = (double)stime.tv_sec + (double)stime.tv_usec/1e6;
  printf("User: %f, System: %f, CPU %f\n", utimef, stimef, utimef+stimef);
}


double diversity(hashmap& m, vector<double> &count, double preSum){
  for (hashmap::const_iterator it = m.begin(); it != m.end(); it++) {
    int id = it->first;
    double c = count[id]; // it->first = feature id
    preSum += (sqrt(it->second + c) - sqrt(c));   // it->second = m(f,a) value 
  }
  return preSum;
} 

double
ReadSentenceCosts(vector<double> &costs, char *filename) {

  double totalCost = 0.0;
  double tmpd;

  if (filename == NULL) 
    error("Need cost file name\n");
  
  string costFile(filename);
  FileReader cFile(costFile); 

  string line;
  
  while (cFile.getInput(line)) {
    tmpd = atof(line.c_str());
    costs.push_back(tmpd);
    totalCost += tmpd;
  }
  if (costs.size() != nUtt) {
    fprintf(stderr,"Expected %i entries in cost file but found %li\n",nUtt,costs.size());
    exit(-1);
  }
  return totalCost;
}


void
ReadFeatureWeights(FeatureMap &weights, char *filename) {

  string weightFile(filename);

  FileReader wFile(weightFile);

  printlines();
  printf("Reading feature weights from %s\n", filename);

  string line;

  for (unsigned int i=0; i<nFeat; i++) {
    wFile.getInput(line);
    weights[i] = atof(line.c_str());
  }
  wFile.clear();
  assert(weights.size() == nFeat);
}

unsigned int
ReadFeatureStats(vector<float> &idfs, vector<float> &fr, char *filename) {

  float ratio,idf,trainc,testc;
  string fname;
  unsigned int id;
  string statFile(filename);
  FileReader sFile(statFile);

  printlines();
  printf("Reading feature stats from %s, %li features\n", filename,idfs.size());

  string line,fstr;
      
  while (sFile.getInput(line)) { 
    stringstream ss(line);
    ss >> id >> fstr >> trainc >> testc >> ratio >> idf ;  
    if (id > idfs.size()) 
      error("ReadFeatureStats: Feature index too large\n");
    if (ratio < 0.0)
      error("ReadFeatureStats: Negative count ratio for feature %i: %f!\n",id,ratio);
    if (idf < 0.0)
      error("ReadFeatureStats: Negative count ratio for feature %i: %f!\n",id,idf);
    fr[id] = ratio; 
    idfs[id] = idf;
  }
  sFile.clear();
  return idfs.size();

}

void
LoadGroundSet(char *fname, priority_queue<Increment> &pq, vset &data, vector<float> &idfs, FeatureMap &weights, vector<double> &conf, vector<double> &costs, vector<double> &preCompute, double preSum,unsigned int nV, 
	      double pf) {

  printf("Reading feature/sentence count matrix\n");

  double value = 0.0;
  unsigned int i = 0;
  unsigned int K = 0;

  printf("Loading ground set\n");
  
  string featureFile(trainFileName); 
  FileReader iFile(featureFile); 

  bool (*fn_pt)(double,double) = cmp;
  std::multimap<double,unsigned int,bool(*)(double,double)> topK (fn_pt);
  std::multimap<double,unsigned int>::iterator it;

  if (prune_factor < 100.0) {
    K  = (int)(prune_factor/100 * nUtt); 
    cout << "Pruning ground set to size " << K << endl;
  }

  string tmp;

  while (iFile.getInput(tmp)) { 

    hashmap sentFea;
    vector<string> fields;
    size_t key;
    
    boost::split( fields, tmp, boost::algorithm::is_any_of( ": " ),boost::algorithm::token_compress_on);
    fields.pop_back();
    
    
    for(std::vector<string>::iterator ist = fields.begin(); ist < fields.end(); ++ist) {
      std::istringstream sstream(*ist);

      sstream >> key; 
      ist++;

      std::istringstream sstream2(*ist);
      sstream2 >> value; 


      double tf = 0.0, tfidf= 0.0;


      if (useTFIDF) { 
	  tf = (double) value / costs[i]; 
	  tfidf = idfs[key] * tf; 
	  value *= tfidf;
      }


      if (useWeights)
	value *= weights[key];

      if (value != 0.0) 
	sentFea[key] = value; 
    }
    
    // compute entire sample's value

    double div = 0.0, norm = 0.0, val = 0.0;

    div = diversity(sentFea,preCompute,preSum);
    norm = pow(costs[i],alpha); 
    val = div / norm; 


    if (prune_factor < 100.0) {

      if (i < K) {
	topK.insert(std::pair<double,unsigned int>(val,i));
	data.emplace(std::pair<unsigned int,hashmap>(i,sentFea));
      }
      else {
	
	it = topK.end(); it--;

	if (val > it->first) {

	  topK.insert(std::pair<double,unsigned int>(val,i));
	  data.emplace(std::pair<unsigned int,hashmap>(i,sentFea));

	  // remove lowest-scoring element from data set

	  it = topK.end(); it--; 
	  unsigned int id = it->second;
	  topK.erase(it);
	  vset::iterator hit = data.find(id);
	  if (hit != data.end())
	    data.erase(hit);
	  else
	    printf("Can't find data entry with id %i\n",id);
	}
      }
    }
    else { 
      pq.push(Increment(val,i)); 
      data.emplace(std::pair<unsigned int,hashmap>(i,sentFea));
    }
    if (i % 100000 == 0)
      printf("%i\n",i);
    i++;
  }
  iFile.clear();
  assert(i == nV);

  cout << "Inserted " << data.size() << " data points out of " << nV << endl;
  
  if (prune_factor < 100.0)  {
    for (it = topK.begin(); it != topK.end(); it++) {
      pq.push(Increment(it->first,it->second));
    }
  }
} 
    


int main(int argc, char** argv){
	
  struct rusage rus; /* starting time */
  struct rusage rue; /* ending time */

  getrusage(RUSAGE_SELF,&rus);
    
  bool parse_was_ok = Arg::parse(argc,(char**)argv);
  if (!parse_was_ok){
    Arg::usage(); exit(-1);
  }

  if (useTFIDF && statFileName == NULL)
    error("Need feature stats file to use TF-IDF weighting\n");

  assert(beta > 0 && beta <= 1.0);

  string tmp2;
    
  vector<double> costs; 
  vector<double> conf(nUtt,1.0); 

  FeatureMap weights;


  vector<float> idfs(nFeat);
  vector<float> ratio(nFeat); 
  vector<double> preCompute(1);
  
  double totalCost = 0;
  double preSum = 0;
  
  printlines();
  
  printf("Reading sentence costs...\n");
  
  totalCost = ReadSentenceCosts(costs,costFileName);
  
  if (confFileName != NULL)
    ReadSentenceCosts(conf,confFileName); 
  
  // set budget

  double maxCost = totalCost*(per/100);
  printf("Total cost of the dataset is %f\n", totalCost);
  printf("Maximum budget for the subset is %f\n", maxCost); 
  
  // read feature stats 

  printf("Reading feature stats...\n");
  if (statFileName != NULL) 
    ReadFeatureStats(idfs,ratio,statFileName);

  long unsigned int nFea = idfs.size();

  // read feature weights

  if (weightFileName != NULL)  {
    ReadFeatureWeights(weights,weightFileName); 
    useWeights=true;
  }

  preCompute.resize(nFea);
  for (long unsigned int i = 0; i < nFea; i++) 
    preCompute[i] = 0;
  

  priority_queue <Increment> rho;
  vset data; 

  printlines();


 LoadGroundSet(trainFileName,rho,data,idfs,weights,conf,costs,preCompute,preSum,nUtt,prune_factor); 

	
    // *********************************************************
    // GREEDY ALGORITHM STARTS HERE
    // *********************************************************
		
    unsigned int nSelected = 0 ; // number of utterances selected
    unsigned int* SelectedSet = new unsigned int[nUtt]; // array to store the selected utt ids

    double maxV = 0;
    double preV = 0;
    double currentCost = 0;

    printlines();

    // accelerated greedy algorithm implementation
    printf("Regularizer of cardinality in knapsack constraint = %f\n", alpha);
    printf("Total number of sentences = %d, percentage to be selected = %f\n",nUtt, per);
    printlines();
    printf("Starting accelerated greedy algorithm\n");


    int sort_cnt = 0;
	
    while (! rho.empty()) {
      int topid = rho.top().get_index();
      rho.pop();

      maxV = diversity(data[topid], preCompute, preSum); 
      
      double newV = (maxV - preV)/pow(costs[topid],alpha);
      
      if (verb >= 4) printf("max gain = %.6e, rho->top() = %.6e\n",newV,rho.top().get_value()); 

      if (newV < (rho.top().get_value()*beta)) { 
	rho.push(Increment(newV, topid));    // if condition not satisfied, push back and re-sort
	sort_cnt++;
	if (verb >= 10) 
	  printf("Condition not met, re-sorting queue (%d iterations)\n",sort_cnt);

      } else {
	// guaranteed to be optimal because of submodularity
	SelectedSet[nSelected++] = topid;
	currentCost += costs[topid];  
	printf("---> Selecting %dth sample (%d selected after %i re-sorts, current normalized increment = %.6e), cost = %f, curCost/budget = %.6e/%.6e, preV = %.6e, curV = %.6e\n",
	       topid, nSelected, sort_cnt,newV,costs[topid],currentCost, maxCost, preV, maxV);
	preV = maxV;
	preSum = maxV;
	
	// update the cluster reward
	hashmap& m = data[topid];
	for (hashmap::const_iterator it = m.begin(); it != m.end(); it++) {
	  preCompute[it->first] = preCompute[it->first] + it->second; 
	}
	sort_cnt = 0;
	if (currentCost >= maxCost) 
	  break;
      }    
    }
    
    printlines();
    printf("Done\n");

    getrusage(RUSAGE_SELF,&rue);
    reportTiming(rus,rue);
	

    // *********************************************************
    // output
    // *********************************************************
    
    FILE* ofp = fopen(outFileName,"w");
    unsigned int s;
    cout << "Printing " << nSelected << " samples\n";
    for(s=0; s < nSelected;s++)
      fprintf(ofp,"%d\n",SelectedSet[s]);
    fclose(ofp);
    printf("Output in %s\n",outFileName);

    return 0;
} 
