#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <ctype.h>

using namespace std;



char *
tokenize_line(char *str, char delim, int *c, int *d) {

  char *cp = str;
  char *words = NULL;
  char *count = NULL;
  
  while (*cp != '\0') {
    if (*cp == delim) {
      if (words == NULL) {
	words = cp+1;
      }
      else if (count != NULL) {
	*c = atoi(count);
      }
      count = cp+1;
      *cp = '\0';
    }
    cp++;
  }
  if (count != NULL) {
    *d = atoi(count);
  }
  else 
    *d = -1;
  return words;
}


int
str2words(char *str, char **words) {

  int s=0,nw=-1;
  char *cp = str;

  while (*cp != '\0') {
    if (isspace(*cp)) {
      if (s == 1) {
	*cp = '\0';
	s = 0;
      }
    }
    else {
      if (s == 0) {
	++nw;
	words[nw] = cp;
	s=1;
      }
    }
    cp++;
  }
  nw++;
  return nw;
}
  

int
CountWords(char *str) {

  char *cp = str;
  int nw = 0;
  int w = 0;
  while (*cp != '\0') {
    if (isspace(*cp)) {
      if (w == 1) 
	w = 0;
    }
    else {
      if (w == 0) {
	nw++;
	w = 1;
      }
    }
    cp++;
  }
  return nw;
}

    

void printlines(){
  fprintf(stderr,"----------------------------------------------\n");
}





  
  
