#Example script for single-step submodular data selection 
#Input: list of training files, test/heldout text file, output directory, budget 

#!/bin/bash 

if [ $# -lt 3 ]
then
    echo "Usage: run.sh <list of train src files> <test file> <budget>" 
    exit
fi

DIR=/g/ssli/projects/afrl/select-lm/software/dsss
# ngram-order 
order=5 

TRAINFILE=$1
TESTFILE=$2 
B=$3

echo $TRAINFILE > train.list
echo $TESTFILE > test.list

#extract query (test) set features

echo "Extracting query set features..."
cmd="$DIR/extract_features -order $order -input test.list -stats $TESTFILE"
echo $cmd
#eval $cmd 

#extract train features
echo "Extracting training features"
cmd="$DIR/extract_features -query $TESTFILE.stats -order $order -input train.list -stats allfeatures" 
echo $cmd
#eval $cmd

NFEA=`wc -l allfeatures.stats | gawk '{print $1}'`
NSENT=`wc -l $TRAINFILE.costs | gawk '{print $1}'` 

#select B% from training file


cmd="$DIR/select_data -n $NFEA -m $NSENT -per $B -out $TRAINFILE.$B.ids -stats allfeatures.stats -costs $TRAINFILE.costs -vset $TRAINFILE.features -alpha 0.2 -fratio -tfidf &> $TRAINFILE.$B.select.log "
echo $cmd
eval $cmd

$DIR/extract_sents -ids $TRAINFILE.$B.ids -input $TRAINFILE -output $TRAINFILE.$B.select

echo "Final output is in $TRAINFILE.$B.select"










  



