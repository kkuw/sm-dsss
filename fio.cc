#include "fio.h"

bool
isgz(string fname) {
  if (fname.find_last_of(".gz") == (fname.length()-1)) 
    return true;
 else 
  return false;
}

void
openInFile(ifstream *iFile, string filename, std::ios_base::openmode m) {

    iFile->open(filename.c_str(),m);
    if (!iFile->is_open()) 
      error("ERROR: cannot open file %s for input\n",filename.c_str());
}

void
openOutFile(ofstream *oFile, string filename, std::ios_base::openmode m) {
   
  oFile->open(filename.c_str(), m);
  if (!oFile->is_open()) 
    error("ERROR: cannot open file %s for output\n",filename.c_str());
}



FileReader::FileReader(string fname) {


  if (isgz(fname)) {    
    isCompressed = true;
    openInFile(&file,fname,std::ios_base::in | std::ios_base::binary);

    gzin.push(boost::iostreams::gzip_decompressor());
    gzin.push(file);
    input = new std::istream(&gzin);
  }
  else {
    openInFile(&file,fname,std::ios_base::in);
    isCompressed = false;
    input = NULL;
  }
  return;
}

void
FileReader::clear() {

  if (file.is_open())
    file.close();
}

FileReader::~FileReader() {

  clear();
  return;
}

istream& 
FileReader::getInput(string &line) {

  if (isCompressed)
    return getline(*input,line);
  else
    return getline(file,line);
}

FileWriter::FileWriter(string fname) {

  
  if (isgz(fname)) {
    isCompressed = true;
    openOutFile(&file,fname,std::ios_base::out | std::ios_base::binary);
    gzout.push(boost::iostreams::gzip_compressor(COMPRESSION_LEVEL));
    gzout.push(file);
  }
  else  {
    openOutFile(&file,fname,std::ios_base::out);
    isCompressed = false;
  }
}

void
FileWriter::close() {

  if (file.is_open())
    file.close();
} 


FileWriter::~FileWriter() {

  if (isCompressed == true)
    gzout.flush();
    boost::iostreams::close(gzout);
  file.close();
}

void
FileWriter::write(string &buf) {

  if (isCompressed)
    gzout.write(buf.c_str(),buf.length()*sizeof(char));
  else
    file.write(buf.c_str(),buf.length());
}

void
FileWriter::write(char *buf) {

  if (isCompressed)
    gzout.write(buf,strlen(buf)*sizeof(char));
  else
    file.write(buf,strlen(buf));
}


  



 
  

















  
  
