
#set BOOSTLIB to Boost library root directory; use version 1_57 or higher
BOOSTLIB=
CC=g++ 
CFLAGS=-Wall -ggdb 
OPT=-O3
OPTIONS=-I$(BOOSTLIB) -L$(BOOSTLIB)
LIBS=-lm -lboost_iostreams 
compile = $(CC) $(CFLAGS) $(OPT) $(OPTIONS)

.c.o:
	$(compile) -o $@ -c $<

.cc.o:
	$(compile) -o $@ -c $<

extract_features: extract_features.o error.o arguments.o fio.o 
	$(compile) -o extract_features $@.o error.o arguments.o fio.o $(LIBS)

select_data: select_data.o error.o arguments.o fio.o 
	$(compile) -o select_data $@.o fio.o error.o arguments.o $(LIBS)

extract_sents: extract_sents.o error.o arguments.o fio.o 
	$(compile) -o extract_sents $@.o fio.o error.o arguments.o $(LIBS)

clean: 
	rm -f *.o extract_features select_data extract_sents

all: extract_features select_data extract_sents
