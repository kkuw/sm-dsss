/* code for extracting n-gram features from text files, for use with select_data
 * Katrin Kirchhoff <kk2@u.washington.edu> */

#include <boost/algorithm/string.hpp>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <stdlib.h>

#include "math.h"
#include "time.h"

#include "error.h"
#include "utils.h"
#include "arguments.h"
#include "fio.h" 

#define MAXBUF 100000
#define MAXLEN 248


char *output_prefix = NULL;
char *fea_file_name = NULL;
char *filelist_name = NULL;
char *help=NULL;
int order = 3; 
bool compressed = false;

typedef boost::unordered_map<string,int> FeatureHash;
typedef boost::unordered_map<unsigned int,unsigned int> FeatureCounter; 

Arg Arg::Args[]={
  Arg("order",Arg::Opt,order,"n-gram order", Arg::SINGLE),
  Arg("query",Arg::Opt,fea_file_name, "file with query set features", Arg::SINGLE),
  Arg("stats",Arg::Req,output_prefix,"output prefix",Arg::SINGLE),
  Arg("input",Arg::Req,filelist_name,"file of input filenames",Arg::SINGLE),
  Arg("help", Arg::Help, help, "Print this message"), 
  Arg()
};

void
WriteFeatureCounts(FeatureHash &ff,vector<string> &feas,vector<unsigned int> &traincounts, vector<unsigned int> &doccounts,FileWriter &stat_file) {

  unsigned int n = feas.size();
  char output[MAXLEN];

  
  if (traincounts.size() < n)
    error("Vector of training counts only has %i entries, expecting %i\n",traincounts.size(),n);

  if (doccounts.size() > 0 && doccounts.size() < n) 
      error("Vector of document counts only has %i entries, expecting %i\n",doccounts.size(),n);

  if (feas.size() < n) 
    error("List of feature only has %i entries, expected %i\n",feas.size(),n);

  for (unsigned int idx = 0; idx < n; idx++) {
    sprintf(output,"%i\t%s\t%i\t%i\n",idx,feas[idx].c_str(),traincounts[idx],doccounts[idx]);
    stat_file.write(output);
  }
  return;
}


// dump feature counts and ratios (for weights) 

void
WriteStats(FeatureHash &ff,vector<string> &feas,vector<unsigned int> &traincounts, 
	   vector<unsigned int> &doccounts, vector<unsigned int> &testcounts, double total_sent, FileWriter &stat_file) {

  
  unsigned int n = feas.size();
  double ratio, idf;
  char output[MAXLEN];
  
  if (traincounts.size() < n)
    error("Vector of training counts only has %i entries, expecting %i\n",traincounts.size(),n);

  if (testcounts.size() > 0 && testcounts.size() < n) 
      error("Vector of test counts only has %i entries, expecting %i\n",testcounts.size(),n);

  if (doccounts.size() > 0 && doccounts.size() < n) 
      error("Vector of document counts only has %i entries, expecting %i\n",doccounts.size(),n);


  if (feas.size() < n) 
    error("List of features only has %i entries, expected %i\n",feas.size(),n);

  for (unsigned int idx = 0; idx < n; idx++) {
    if (traincounts[idx] > 0 && testcounts[idx] > 0)  
      ratio = testcounts[idx]/(double)traincounts[idx];
    else
      ratio = 0.0; 
    if (doccounts[idx] > 0.0)
      idf = log(total_sent/doccounts[idx]);
    else
      idf = 0.0;
    sprintf(output,"%i\t%s\t%i\t%i\t%e\t%e\n",idx,feas[idx].c_str(),traincounts[idx],testcounts[idx],ratio,idf);
    stat_file.write(output);
  }
  return;
}


unsigned int
LoadTestFeatures(string fname, FeatureHash *hashmap, vector<unsigned int> *counts, vector<string> &feas) {

    ifstream file;
    char *ngram;
    int c = 0, d = 0;
    unsigned int n = 0;
    string line;

    openInFile(&file,fname,std::ios_base::in);
    pair<FeatureHash::iterator,bool> ret;

    while (getline(file,line)) { 
      ngram = (char *)tokenize_line((char *)line.c_str(),'\t',&c,&d); 
      string key(ngram);
      ret = hashmap->insert(make_pair(key,n));
      if (ret.second == false)
	error("Insertion into hash failed for %s\n",ngram);
      if (c != -1) 
	counts->push_back((unsigned int)c);
      feas.push_back(key);
      n++;
    }
    cout << "Read " << n << " test set features\n";
    return n;
}


unsigned int
LoadTestFeaturesHash(string fname, vector<unsigned int> &counts) {

    ifstream file;
    //    char *ngram;
    int c = 0, d = 0, id;
    unsigned int n = 0;
    string line;

    openInFile(&file,fname,std::ios_base::in);
    while (getline(file,line)) { 
      sscanf(line.c_str(),"%i\t%i\t%i\n",&id,&c,&d);
      counts[id] = c;
      //      printf("%i %i\n",id,counts[id]);
      n++;
    }
    cout << "Read " << n << " test set features\n";

    return n;
}

void
InsertNewFeature(FeatureHash *ff, vector<string> &flist,FeatureCounter &sentCounts,vector<unsigned int> &trainCounts, string ngram) {

  unsigned int new_id = ff->size();

  ff->insert(make_pair(ngram,new_id)); 

  sentCounts[new_id] = 1;

  if (new_id >= trainCounts.size()) 
    trainCounts.resize(new_id+1);
  trainCounts[new_id] = 1;

  if (new_id >= flist.size()) 
    flist.resize(new_id+1);
  flist[new_id] = ngram;

  return;
}

void
CountFeatures(char *str, vector<unsigned int> &trainCounts, vector<unsigned int> &docCounts, 
	      FeatureHash *ff, vector<string> &flist, string &cbuf, string &feabuf,bool addToHash) {

  int len = strlen(str);
  char **words = new char*[len]; 

  int nw = -1;

  FeatureCounter sentCounts;  // per-sentence count

  /* tokenize into words */

  nw = str2words(str,words);

  FeatureHash::const_iterator ret; 
  FeatureCounter::iterator it;
  char **nwords; 

  nwords = words;

  // read n-gram features

   for (int i=0; i<nw; i++) {
     string ngram(nwords[i]);
     if ((ret = ff->find(ngram)) != ff->end()) {
       sentCounts[ret->second] += 1;
       trainCounts[ret->second] += 1;
     }
     else {
       if (addToHash)
	 InsertNewFeature(ff,flist,sentCounts,trainCounts,ngram);
     }
   
     for (int j = i+1; j < i+order && j < nw; j++) {
       ngram.append("#");
       ngram.append(nwords[j]);
       if ((ret = ff->find(ngram)) != ff->end()) {
	 sentCounts[ret->second] += 1;
	 trainCounts[ret->second] += 1;
       }
       else {
	 if (addToHash) {
	   InsertNewFeature(ff,flist,sentCounts,trainCounts,ngram);
	 }
       }
      } 
   }

  char out[MAXLEN];

  if (docCounts.size() < ff->size()) 
    docCounts.resize((unsigned int)ff->size());

  for (it = sentCounts.begin(); it != sentCounts.end(); it++) {
    sprintf(out,"%i:%i ",it->first,it->second);
    feabuf.append(out);  
    docCounts[it->first] += 1; 
  }
  feabuf.append("\n");
  sprintf(out,"%i\n",nw);
  cbuf.append(out);
  return;
 }


void
ProcessSent(string s, string &coststr, string &feastr, vector<unsigned int> &feacounts, 
	    vector<unsigned int> &dcounts, vector<unsigned int> &testcounts,
	    FeatureHash *features,vector<string> &flist, 
	    FileWriter &output, FileWriter &costs, unsigned int *tc, bool addToHash) {

  unsigned int i = *tc;

  CountFeatures((char *)s.c_str(),feacounts,dcounts,features,flist,coststr,feastr,addToHash); 

  if (i == MAXBUF-1) {
    cout << ".";

    costs.write(coststr);
    output.write(feastr);

    coststr.clear();
    feastr.clear();

    *(tc) = 0; 
  }
  (*tc)++;
  return;
}

void
ClearBuffers(string &wcbuf, string &feabuf, FileWriter &cost_file, FileWriter &out_file) {
  
    cost_file.write(wcbuf);
    out_file.write(feabuf);
    
    wcbuf.clear();
    feabuf.clear();

    return;
}

int
main(int argc, char **argv) {

  string wcbuf;
  string feabuf;

  unsigned int t = 0;


  FeatureHash testFea; // query set features
  vector<string> feaList;

  vector<unsigned int> testcounts;
  vector<unsigned int> traincounts;
  vector<unsigned int> doccounts;

  string cost_fname, out_fname, stat_fname;

  double total_sent = 0;

  bool addToHash = false;

  bool parse_was_ok = Arg::parse(argc,(char**)argv);

  if (!parse_was_ok){
    Arg::usage(); exit(-1);
  }

  // read query set features if available

  if (fea_file_name != NULL) {
    string fea_fname(fea_file_name);
    LoadTestFeatures(fea_fname,&testFea,&testcounts,feaList);
    traincounts.resize(testFea.size());
    doccounts.resize(testFea.size());
  }
  else 
    addToHash = true;


  string input_fname(filelist_name);
  ifstream input_list;
  string op(output_prefix);
  string fname,line;

  openInFile(&input_list,input_fname,std::ios_base::in);


  while (getline(input_list,fname)) {

    cout << fname << endl;

    FileReader input(fname); 

    out_fname =  fname + ".features";
    cost_fname = fname + ".costs";

    if (compressed) {
      out_fname += ".gz";
      cost_fname += ".gz";
    }

    cout << "Feature output in " << out_fname.c_str() << endl;
    cout << "Cost output in " << cost_fname.c_str() << endl;

    FileWriter feaOut(out_fname);
    FileWriter costOut(cost_fname);

    while (input.getInput(line)) { 
      ProcessSent(line,wcbuf,feabuf,traincounts,doccounts,testcounts,&testFea,feaList,feaOut,costOut,&t,addToHash);
      total_sent++;
    }
    ClearBuffers(wcbuf,feabuf,costOut,feaOut);
    input.clear();
    cout << "Done" << endl;
  }
  stat_fname = op + ".stats";
  FileWriter statOut(stat_fname);

  if (fea_file_name != NULL) 
    WriteStats(testFea,feaList,traincounts,doccounts,testcounts,total_sent,statOut);
  else
    WriteFeatureCounts(testFea,feaList,traincounts,doccounts,statOut);    
}























